# Stingray Low Level Controller Package
This package contains the real-world triton robot with camera package. 

## Installation

### Install the Legacy Arduino IDE
The Arduino code in this repository was originally developed with the legacy Arduino 1.X IDE and was not tested with the new Arduino 2.X IDE. Furthermore, only the legacy Arduino IDE is officially compiled and packaged by the Arduino software maintainers for the ARM instruction set, and as such, cannot run directly on the Triton robots (which has a Jetson Nano with an ARM processor). Therefore, it is recommended to install the last legacy Arduino IDE v1.8.19: https://www.arduino.cc/en/software#:~:text=IDE%20(1.8.X)-,Arduino%20IDE%201.8.19,-The%20open%2Dsource

### Install the Required Arduino libraries
There are four dependency libraries of this Arduino code that is not installed by default. Install the four packages listed below: 
- Adafruit NeoPixel v1.8.7
- ArduinoThread v2.1.1
- PinChangeInterrupt v1.2.9
- Rosserial Arduino Library v0.7.8
Note that the version for Rosserial Arduino Library must be exactly 0.7.8 since the newer versions will not compile properly. 
![Screenshot](docs/img/arduino.png)

### Download the Stingray Arduino Repository
```bash
git clone https://gitlab.com/HCRLab/stingray-robotics/stingray_arduino.git
```

### Install Stingray Arduino Code
Open the `llc.ino` file from within the Arduino IDE. Connect the Arduino to your computer (or to the Jetson Nano on the Triton robot) using a USB type A to USB type B cable. In the Arduino IDE menu under Tools->Board, select Arduino Mega or Mega 2560. Under Tools->Port, select `/dev/ttyACM0` (usually) unless there you see that a different port appeared upon connecting the Arduino. Then select the "Upload" icon the program the Arduino. This should only take a few seconds.  

## Description
This Arduino code implements the low level controller (LLC) for the Triton robot. It interfaces  using the `rosserial` library to connect to the high level controller (HLC) running ROS on the Jetson Nano. 

### ROS Topics

The LLC subscribes to two ROS topics: 
- `/cmd_vel`: Robot commands of type `geometry_msgs/Twist` are received on this topic. The LLC uses only the `cmd.linear.x`, `cmd.linear.y`, and `cmd.angular.z` values since the Triton robot is a 2-dimensional omnidirectional robot. 
- `/cmd_color`: Commands of type `std_msgs/Int32` are received on this topic. The LLC interprets these integers as RGB hex values, where `0xFF0000` corresponds to RED, `0x00FF00` corresponds to GREEN, and `0x0000FF` corresponds to BLUE. This is used to set the color of the LED ring on top of the Triton robot, as well as an RGB LED inside the robot. 

The LLC also publishes a TF transform: 
- `odom_to_base_link`: Based on motor encoder information, the LLC publishes its current belief of the robot's pose as its odometry reading. Only a TF transform is published, and not a `/odom` topic since the standard ROS packages only usually utilize the TF transform. However, it is possible to extend this Arduino code to also publish on the `/odom` topic. 

### Kinematics

Because the Triton robot is an omnidirectional robot with three wheels, it is nontrivial to convert velocity commands to motor commands, as well as motor speeds to odometry. A good reference for this kind of robot is discussed in Chapter 13.2 (Omnidirectional Wheeled Mobile Robots) in the book Modern Robotics by Lynch & Park (2017). There are some slight variations as the configuration of the Triton robot's wheels with respect to the robot's frame is different, but the same general principles apply. 

![Screenshot](docs/img/omnirobots1.png)
![Screenshot](docs/img/omnirobots2.png)
![Screenshot](docs/img/omnirobots3.png)
![Screenshot](docs/img/omnirobots4.png)

### Code Structure

The main LLC logic and code can be found in `llc.ino`. This is a regular Arduino source code that has a thread for the `rosserial` connection with the HLC, callback functions for the `/cmd_vel` and `/cmd_color` topics for receiving the ROS messages, and a thread for `updateSpeed` for actually forwarding the velocity commands to the PID motor controllers as well as computing odometry values that are broadcasted as a TF transform. 

The `driveController.cpp` file contains the source code implementing the forward kinematics of the Triton robot. It includes functions for setting the PWM values for each motor based on the computed velocities by the PID controllers as well as interrupt code whenever the motor encoder values change (for tracking wheel motion). 

The `lightRing.cpp` file contains minimal source code for controlling the LED ring using the `Adafruit_NeoPixel` library. Here, the single color command from ROS is used to set all LEDs in the ring; however, it is also possible to set them individually. 

The `pid.cpp` file contains the PID controller code that allows the robot to set motor velocities. The input of the `PID_getControlValue()` function is the current velocity and target velocity which is used to compute the velocity error. The output is a change in the PWM motor command computed using the error and the proportional (P), integral (I), and derivative (D) constants. 
