#include "driveController.h"  

#include <Arduino.h>
#include <PinChangeInterrupt.h>
#include <math.h>
#include "pid.h"

//helper defines
#define r2d(x) ((x) * (180.0 / PI))
#define d2r(x) ((x) * (PI / 180.0))
#define GEAR_RATIO 62.5
#define COUNTS_PER_BACKSHAFT_REV 20
#define COUNTS_PER_REV (COUNTS_PER_BACKSHAFT_REV * GEAR_RATIO)

//ENUMERATIONS
enum MOTOR_IDS {LEFT, RIGHT, BACK, NUM_MOTORS};
enum MOTOR_PIN_TYPE {DIR, PWM};
enum ENC_TYPE {A, B}; 

//CONSTANT STATIC VARIABLES
//pin definitions
const static uint8_t MOTOR_PINS[NUM_MOTORS][2] = { {2, 3},     //Left
                                             {4, 5},     //Right
                                             {6, 7} };   //Back
const static uint8_t ENC_PINS[NUM_MOTORS][2] = { {50, 51},   //Left
                                           {52, 53},   //Right
                                           {A8, A9} }; //Back


//linear control values 
const static float K_P = 200.0;
const static float K_I = 0.5;
const static float K_D = 0.1;
const static uint8_t MIN_SPEED = 0;  //pwm
const static uint8_t MAX_SPEED = 255; //pwm
const static int16_t STOP_THRESHOLD = 1;//5; //pwm

//physical specs
const static float WHEEL_DIAMETER = 2.3 * 2.54 / 100; //inches to meters
const static float WHEEL_CIRCUMFERENCE = (WHEEL_DIAMETER * PI); //meters
const static float ROBOT_RADIUS = 3.9 * 2.54 / 100; //inches to meters
const static float ROBOT_CIRCUMFERENCE = (ROBOT_RADIUS * 2 * PI);
const static float WHEEL_ANGLES[NUM_MOTORS] = {d2r(150), d2r(30), d2r(270)};

static float ROTSPEED2SPEED(float x) {
  return x * ROBOT_RADIUS;
}

//STATIC MODULE MEMBERS
static PID controller[NUM_MOTORS];
static float current_motor_setpoints[NUM_MOTORS] = {0};
static volatile long long encCounts[NUM_MOTORS] = {0};
static volatile long long lastEncCounts[NUM_MOTORS] = {0};
static float x_speed = 0, y_speed = 0;
static float r_speed = 0;
static int update_rate_ms;

//private method declarations
static void DriveController_setMotorSpeed(int m, int speed );
static float DriveController_getMotorSpeed(int m);

//encoder methods
static void DriveController_encoderCount(int enc, int type);
static void DriveController_resetEncoderCounts();
static void DriveController_LA_changed();
static void DriveController_LB_changed();
static void DriveController_RA_changed();
static void DriveController_RB_changed();
static void DriveController_BA_changed();
static void DriveController_BB_changed();


//method definitions

static void DriveController_init(uint16_t _update_rate_ms) {
    update_rate_ms = _update_rate_ms;
    for (int m = 0; m < NUM_MOTORS; m++){
        controller[m].kp = K_P;
        controller[m].ki = K_I;
        controller[m].kd = K_D;
        controller[m].min = MIN_SPEED;
        controller[m].max = MAX_SPEED;
        controller[m].dt_ms = update_rate_ms;
        controller[m].last_error = 0;
        controller[m].integral = 0;
    }

    for (int m = 0; m < NUM_MOTORS; m++) {
        pinMode(ENC_PINS[m][A], INPUT_PULLUP);
        pinMode(ENC_PINS[m][B], INPUT_PULLUP);
        pinMode(MOTOR_PINS[m][DIR], OUTPUT);
        pinMode(MOTOR_PINS[m][PWM], OUTPUT);
        DriveController_setMotorSpeed(m, 0);
    }
    //attach interrupts to encoder pins
    attachPCINT(digitalPinToPCINT(ENC_PINS[LEFT] [A]), DriveController_LA_changed,  CHANGE);
    attachPCINT(digitalPinToPCINT(ENC_PINS[LEFT] [B]), DriveController_LB_changed,  CHANGE);
    attachPCINT(digitalPinToPCINT(ENC_PINS[RIGHT][A]), DriveController_RA_changed,  CHANGE);
    attachPCINT(digitalPinToPCINT(ENC_PINS[RIGHT][B]), DriveController_RB_changed,  CHANGE);
    attachPCINT(digitalPinToPCINT(ENC_PINS[BACK] [A]), DriveController_BA_changed,  CHANGE);
    attachPCINT(digitalPinToPCINT(ENC_PINS[BACK] [B]), DriveController_BB_changed,  CHANGE);
}

static void DriveController_update(float motor_speed[]){
    if (x_speed == 0 && y_speed == 0 && r_speed == 0){
      for (int m = 0; m < NUM_MOTORS; m++){
        float current_speed = DriveController_getMotorSpeed(m);
        DriveController_setMotorSpeed(m, 0);
        controller[m].last_error = 0;
        controller[m].integral = 0;
        current_motor_setpoints[m] = 0;
        motor_speed[m] = current_speed;
      }
      DriveController_resetEncoderCounts();
      return;
    }
  
    for (int m = 0; m < NUM_MOTORS; m++){
        float motor_target = cos(WHEEL_ANGLES[m]) * x_speed + 
                             sin(WHEEL_ANGLES[m]) * y_speed + 
                             ROTSPEED2SPEED(r_speed);
        float current_speed = DriveController_getMotorSpeed(m);
        motor_speed[m] = current_speed;
        float motor_val = PID_getControlValue(&controller[m], current_speed, motor_target);
        current_motor_setpoints[m] += motor_val;
        current_motor_setpoints[m] = min(current_motor_setpoints[m], MAX_SPEED);
        current_motor_setpoints[m] = max(current_motor_setpoints[m], -MAX_SPEED);
    }
    for (int m = 0; m < NUM_MOTORS; m++)
        DriveController_setMotorSpeed(m, current_motor_setpoints[m]);
}

static void DriveController_setLinearVelocity(float x, float y){
    // DriveController_resetEncoderCounts();
    x_speed = x;
    y_speed = y;
}

static void DriveController_setAngularVelocity(float vel_r){
    // DriveController_resetEncoderCounts();
    r_speed = vel_r;
}

static void DriveController_setMotorSpeed(int m, int speed ){
    if (abs(speed) < STOP_THRESHOLD) speed = 0;
    digitalWrite(MOTOR_PINS[m][DIR], speed >= 0 ? HIGH:LOW );
    analogWrite(MOTOR_PINS[m][PWM], abs(speed));
}

static float DriveController_getMotorSpeed(int m) {
    int diff = encCounts[m] - lastEncCounts[m];
    lastEncCounts[m] = encCounts[m];
    return ((float) diff / COUNTS_PER_REV) * WHEEL_CIRCUMFERENCE / (update_rate_ms / 1000.0);
}

static void DriveController_encoderCount(int enc, int type) {
  
    if (type == A) {
        if (digitalRead(ENC_PINS[enc][A]) == HIGH)
            if (digitalRead( ENC_PINS[enc][B]) == LOW)  encCounts[enc] ++;
            else                                        encCounts[enc] --;
        else
            if (digitalRead( ENC_PINS[enc][B]) == HIGH) encCounts[enc] ++;
            else                                        encCounts[enc] --;
    }
  
    else if (type == B) {
        if (digitalRead(ENC_PINS[enc][B]) == HIGH)
            if (digitalRead( ENC_PINS[enc][A]) == HIGH) encCounts[enc] ++;
            else                                        encCounts[enc] --;
        else
            if (digitalRead( ENC_PINS[enc][A]) == LOW)  encCounts[enc] ++;
            else                                        encCounts[enc] --;
    }
}

static void DriveController_resetEncoderCounts(){
    for (int m = 0; m < NUM_MOTORS; m++){
      lastEncCounts[m] = 0;
      encCounts[m] = 0;
    }
}
/*
   A series of void arg void callback methods that pass arguments needed to
   the actual interrupt function since pinchange interrupts do not allow argument passing
*/
static void DriveController_LA_changed() {
  DriveController_encoderCount(LEFT, A);
}
static void DriveController_LB_changed() {
  DriveController_encoderCount(LEFT, B);
}
static void DriveController_RA_changed() {
  DriveController_encoderCount(RIGHT, A);
}
static void DriveController_RB_changed() {
  DriveController_encoderCount(RIGHT, B);
}
static void DriveController_BA_changed() {
  DriveController_encoderCount(BACK, A);
}
static void DriveController_BB_changed() {
  DriveController_encoderCount(BACK, B);
}
