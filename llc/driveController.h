#include "pid.h"
#include <Arduino.h>

#ifndef _DRIVE_CONTROLLER_H_
#define _DRIVE_CONTROLLER_H_

//method declarations
extern void DriveController_init(uint16_t _update_rate_ms);
extern void DriveController_setLinearVelocity(float vel_x, float vel_y);
extern void DriveController_setAngularVelocity(float vel_r);
extern void DriveController_update(float motor_speed[]);


#endif //_DRIVE_CONTROLLER_H_
