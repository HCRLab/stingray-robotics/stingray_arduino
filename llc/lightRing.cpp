#include "lightRing.h"
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

static const uint8_t NEO_PIN = 8;
static const uint16_t NEO_BRIGHTNESS = 30;

static Adafruit_NeoPixel ring = Adafruit_NeoPixel(NUM_LIGHTS, NEO_PIN,NEO_GRB + NEO_KHZ800);
static uint8_t brightness;
static uint32_t color;

static void LightRing_init(){
     pinMode(NEO_PIN, OUTPUT);
     ring.begin();
     ring.setBrightness(NEO_BRIGHTNESS);
     LightRing_setColor(BLUE);
     ring.show();
}

static void LightRing_setColor(uint32_t _color){
     color = ring.Color(_color >> 16, (_color >> 8) & 0xFF, _color & 0xFF);
     ring.fill(color);
}

static void LightRing_update(){
    ring.show();
}
