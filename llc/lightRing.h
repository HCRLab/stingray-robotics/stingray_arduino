
#ifndef _LIGHT_RING_H_
#define _LIGHT_RING_H_

#include <stdint.h>

#define NUM_LIGHTS 60
#define OFF   0x000000
#define RED   0xFF0000
#define GREEN 0x00FF00
#define BLUE  0x0000FF
#define ON    0xFFFFFF
#define DEG_TO_LIGHT(x) (round((float)x / NUM_LIGHTS))

extern void LightRing_init();
extern void LightRing_setColor(uint32_t color);
extern void LightRing_update();

#endif 
