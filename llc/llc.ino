#include <math.h>
#include <stdint.h>

#include <ThreadController.h>
#include <Thread.h>

#include <ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Int32.h>
#include <ros/time.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include "driveController.h"
#include "lightRing.h"
#include "tests.h"

//helper defines
#define r2d(x) ((x) * (180.0 / PI))
#define d2r(x) ((x) * (PI / 180.0))

ThreadController tc;
const uint16_t update_rate_drive_controller_ms = 25;
const uint16_t update_rate_light_ring_ms = 100;
volatile unsigned long lastUpdate = 0;
volatile float odom_x, odom_y, odom_th, odom_vx, odom_vy, odom_w;
const static float ROBOT_RADIUS = 3.9 * 2.54 / 100; //inches to meters

// ros::NodeHandle_<ArduinoHardware, 25, 25, 1024, 512> nh;
ros::NodeHandle nh;

void drive_cmd_callback(const geometry_msgs::Twist& cmd) {
  DriveController_setLinearVelocity(cmd.linear.x, cmd.linear.y);
  DriveController_setAngularVelocity(cmd.angular.z);
  lastUpdate = millis();
} ros::Subscriber<geometry_msgs::Twist> drive_cmd("cmd_vel", &drive_cmd_callback);

void color_cmd_callback( const std_msgs::Int32& cmd) {
  LightRing_setColor(cmd.data);
} ros::Subscriber<std_msgs::Int32> color_cmd("cmd_color", &color_cmd_callback);

geometry_msgs::TransformStamped t;
tf::TransformBroadcaster broadcaster;
char base_link[] = "/base_link";
char odom[] = "/odom";

void monitorROS() {
  if (!nh.connected()) {
    DriveController_setLinearVelocity(0, 0);
    DriveController_setAngularVelocity(0);
    LightRing_setColor(RED);
    updateSpeed();
    LightRing_update();
  }
  unsigned long lastTime = lastUpdate;
  unsigned long currTime = millis();
  if (currTime > lastTime + 1000) {
    DriveController_setLinearVelocity(0, 0);
    DriveController_setAngularVelocity(0);
    lastUpdate = currTime;
  }
}

void updateSpeed() {
    float motor_speed[3] = {0.0};
    DriveController_update(motor_speed);
    // odometry equations for 3-wheeled omni robot from Modern Robotics by Lynch & Park, 2017
    odom_w = (1/(3 * ROBOT_RADIUS)) * (motor_speed[0] + motor_speed[1] + motor_speed[2]);
    odom_vx = (1 / (2 * sin(PI/3))) * (-motor_speed[0] + motor_speed[1]);
    odom_vy = (1.0/3) * (motor_speed[0] + motor_speed[1]) + (-2.0/3) * (motor_speed[2]);

    odom_x += (odom_vx*cos(odom_th)+odom_vy*cos(odom_th+PI/2)) * update_rate_drive_controller_ms / 1000.0;
    odom_y += (odom_vx*sin(odom_th)+odom_vy*sin(odom_th+PI/2)) * update_rate_drive_controller_ms / 1000.0;
    odom_th += odom_w * update_rate_drive_controller_ms / 1000.0;
    
    t.header.frame_id = odom;
    t.child_frame_id = base_link;
    t.transform.translation.x = odom_x;
    t.transform.translation.y = odom_y;
    t.transform.rotation = tf::createQuaternionFromYaw(odom_th);
    t.header.stamp = nh.now();
    broadcaster.sendTransform(t);
}

void makeThread(void (*target_function)(void), uint16_t update_rate_ms) {
  Thread* t = new Thread();
  t->onRun(target_function);
  t->setInterval(update_rate_ms);
  tc.add(t);
}

void setup() {
  odom_x = odom_y = odom_th = odom_vx = odom_vy = odom_w = 0;

  Serial.begin(115200);

  DriveController_init(update_rate_drive_controller_ms);
  makeThread(updateSpeed, update_rate_drive_controller_ms);

  LightRing_init();
  makeThread(LightRing_update, update_rate_light_ring_ms);

  makeThread(monitorROS, 50.0);

  nh.getHardware()->setBaud(115200);
  nh.initNode();
  nh.subscribe(drive_cmd);
  nh.subscribe(color_cmd);
  broadcaster.init(nh);

  LightRing_setColor(BLUE);
  while (!nh.connected()) {
    nh.spinOnce();
  }
  LightRing_setColor(GREEN);
  Serial.println("Stingray Started.");
}

void loop() {
  tc.run();
  nh.spinOnce();
}
