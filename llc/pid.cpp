#include "pid.h"

static float PID_getControlValue (PID* pid, float current, float target){
    
    float error = target - current;
    float p = pid->kp * error;

    pid->integral += error * (pid->dt_ms);
    if (pid->integral > pid->max) pid->integral = pid->max;
    if (-pid->integral < -pid->max) pid->integral = -pid->max;
    float i = pid->ki * pid->integral;

    float deriv = (error - pid->last_error) / (pid->dt_ms / 1000.0);
    float d = pid->kd * deriv;

    pid->last_error = error;

    float output =  p + i + d;

    if (output >  pid->max/4) return pid->max/4;
    if (output < -pid->max/4) return -pid->max/4;
    return output;
}
