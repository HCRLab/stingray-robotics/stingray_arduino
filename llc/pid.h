
#ifndef _PID_H_
#define _PID_H_

typedef struct {
    float kp, ki, kd, max, min;
    int dt_ms;
    double integral;
    double last_error;
} PID;

extern float PID_getControlValue (PID* pid, float current, float target);

#endif //_PID_H_
