#include "driveController.h"
#include <math.h>

#ifndef _TESTS_H_
#define _TESTS_H_

#define r2d(x) ((x) * (180.0 / PI))
#define d2r(x) ((x) * (PI / 180.0))

void example_circle(){

    // go in a circle test
    static float deg = 0;

    float speed = 9;
    float x = speed * cos(d2r(deg));
    float y = speed * sin(d2r(deg));

    DriveController_setLinearVelocity(x, y);
    DriveController_setAngularVelocity(0);
  
    deg += 1;

}
void example_sin(){
    
    // go in a circle test
    static float deg = 0;

    float speed = 9.0;
    float x = speed;
    float y = speed * sin(d2r(deg));

    DriveController_setLinearVelocity(x, y);
    DriveController_setAngularVelocity(0);
    
    deg += 1;
}

#endif //_TESTS_H_
